# Learning

https://www.metacademy.org/roadmaps/cjrd/level-up-your-ml

- [ ] **Math**
    - [ ] Convex Optimization
     * Convex Optimization / Boyd [Stanford / Lectures](http://stanford.edu/class/ee364a/index.html) / [Book](http://stanford.edu/~boyd/cvxbook/bv_cvxbook.pdf)


- [ ] **Machine Learning**
    - [ ] Probabilistic Modeling
         * Probabilistic Graphical Models [Stanford / Coursera](http://bit.ly/stanford-pgm)